import 'package:flutter/material.dart';
import '../models/avion.dart';

class AvionWidget extends StatelessWidget {
  final Avion avion;
  final Function onTap;
  final Function onSwipe;
  final Function onLongPress;

  const AvionWidget(
      {Key? key,
      required this.avion,
      required this.onTap,
      required this.onSwipe,
      required this.onLongPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
        onSwipe(avion.id);
      },
      onLongPress: () {
        onLongPress(avion.id);
      },
      onTap: () {
        onTap(avion);
      },
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 80.0,
        child: ListTile(
          title: Text(avion.modelo),
          subtitle: Text(avion.tipo),
          leading: Image.network(avion.fotoUrl),
          trailing: Icon(Icons.arrow_forward_rounded),
        ),
      ),
    );
  }
}
