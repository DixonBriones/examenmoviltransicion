import 'package:flutter/material.dart';

import '../constans.dart';
import '../models/avion.dart';
import 'listScreen.dart';

class DetailScreen extends StatelessWidget {
  final Avion avion;

  const DetailScreen({Key? key, required this.avion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(avion.modelo),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.network(avion.fotoUrl),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Capacidad de tripulantes: ${avion.capacidad}',
                    textAlign: TextAlign.justify, style: textStyle),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Tipo de avion: ${avion.tipo}',
                    textAlign: TextAlign.justify, style: textStyle),
              ),
              ElevatedButton(
                  onPressed: (() {
                    Navigator.pop(context);
                  }),
                  child: Text(
                    "Volver al inicio",
                    style: textStyle,
                  ))
            ],
          ),
        ));
  }
}
