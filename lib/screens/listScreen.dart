import 'package:flutter/material.dart';

import '../constans.dart';
import '../models/backend.dart';
import '../models/avion.dart';
import '../widgets/avion_widget.dart';
import 'detailScreen.dart';

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  var avions = Backend().getAviones();

  void deleteAvion(int id) {
    Backend().deleteAvion(id);
    setState(() {
      avions = Backend().getAviones();
    });
  }

  void showDetail(Avion avion) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DetailScreen(avion: avion);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: avions.length,
        itemBuilder: (BuildContext context, int index) => AvionWidget(
            avion: avions[index],
            onTap: showDetail,
            onLongPress: showDetail,
            onSwipe: deleteAvion),
      ),
    );
  }
}
