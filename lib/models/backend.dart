import 'avion.dart';

class Backend {
  static final Backend _backend = Backend._internal();

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  final _avions = [
    Avion(
        id: 1,
        modelo: 'Boeing 777',
        capacidad: '550',
        tipo: 'Comercial',
        fotoUrl:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Cathay_Pacific_Boeing_777-200%3B_B-HNL%40HKG.jpg/1200px-Cathay_Pacific_Boeing_777-200%3B_B-HNL%40HKG.jpg'),
    Avion(
        id: 2,
        modelo: ' F-22 Raptor',
        capacidad: '1',
        tipo: 'Militar',
        fotoUrl:
            'https://upload.wikimedia.org/wikipedia/commons/4/46/Lockheed_Martin_F-22A_Raptor_JSOH.jpg'),
    Avion(
        id: 3,
        modelo: 'Airbus A320',
        capacidad: '220',
        tipo: 'Comercial',
        fotoUrl:
            'https://upload.wikimedia.org/wikipedia/commons/c/c6/Airbus_A320-214%2C_China_Eastern_Airlines_JP6480919.jpg'),
    Avion(
        id: 4,
        modelo: 'Boeing 757',
        capacidad: '289',
        tipo: 'Comercial',
        fotoUrl:
            'https://upload.wikimedia.org/wikipedia/commons/3/31/Icelandair_Boeing_757-200_Wedelstaedt.jpg'),
    Avion(
        id: 5,
        modelo: 'Airbus A300',
        capacidad: '266',
        tipo: 'Comercial',
        fotoUrl:
            'https://upload.wikimedia.org/wikipedia/commons/2/2a/Airbus_A300B4-605R%2C_Iran_Air_AN0507687.jpg')
  ];

  List<Avion> getAviones() {
    return _avions;
  }

  void deleteAvion(int id) {
    _avions.removeWhere((avion) => avion.id == id);
  }
}
