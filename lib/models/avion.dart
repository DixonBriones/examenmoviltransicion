class Avion {
  final int id;
  final String modelo;
  final String capacidad;
  final String tipo;
  final String fotoUrl;

  Avion(
      {required this.id,
      required this.modelo,
      required this.capacidad,
      required this.tipo,
      required this.fotoUrl});
}
